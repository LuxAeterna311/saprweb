#FROM openjdk:11
#ADD target/saprweb-0.0.1-SNAPSHOT.jar app.jar
#ENTRYPOINT ["java", "-jar", "/app.jar"]

FROM openjdk:11
WORKDIR /usr/src/app
EXPOSE 4000
COPY . .
RUN mvn clean package
CMD ["java", "-jar", "target/saprweb-0.0.1-SNAPSHOT.jar"]