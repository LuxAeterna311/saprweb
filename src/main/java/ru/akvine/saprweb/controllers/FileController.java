package ru.akvine.saprweb.controllers;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.akvine.saprweb.services.FileService;

import java.io.IOException;

@Controller
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequestMapping("/files")
public class FileController {
    FileService fileService;

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping()
    public String getFilesPage() {
        return "files_page";
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("file") MultipartFile multipartFile) throws IOException, ClassNotFoundException {
        fileService.upload(multipartFile);

        return "redirect:/files";
    }

    @GetMapping("/download")
    public String downloadFile(@RequestParam(value = "file_name", defaultValue = "") String fileName) throws IOException {
        fileService.download(fileName);

        return "redirect:/files";
    }

}
