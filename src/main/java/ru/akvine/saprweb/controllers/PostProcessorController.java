package ru.akvine.saprweb.controllers;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.akvine.saprweb.domain.models.PostProcessorProperties;
import ru.akvine.saprweb.services.PostProcessorService;

@Controller
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PostProcessorController {

    PostProcessorService postProcessorService;

    PostProcessorProperties postProcessorProperties;

    @Autowired
    public void setPostProcessorProperties(PostProcessorProperties postProcessorProperties) {
        this.postProcessorProperties = postProcessorProperties;
    }

    @Autowired
    public void setPostProcessorService(PostProcessorService postProcessorService) {
        this.postProcessorService = postProcessorService;
    }

    @GetMapping("/postprocessor")
    public String getPostprocessorPage(@RequestParam(value = "rodNumber", defaultValue = "1") String rodNumber,
                                       Model model) {
        double[][] matrixNx = postProcessorService.calculateNx(Integer.parseInt(rodNumber) - 1);
        double[][] matrixUx = postProcessorService.calculateUx();
        double[][] sigma = postProcessorService.calculateSigma(Integer.parseInt(rodNumber) -1 );

        model.addAttribute("matrixNx", matrixNx);
        model.addAttribute("matrixUx", matrixUx);
        model.addAttribute("sigma", sigma);

        return "postprocessor_page";
    }

    @GetMapping("/recalculation")
    public String recalculate(@RequestParam(value = "nx_rows_count", defaultValue = "10") String nxRowsCount,
                              @RequestParam(value = "nx_rod_number", defaultValue = "1") String nxRodNumber,
                              @RequestParam(value = "ux_rows_count", defaultValue = "10") String uxRowsCount,
                              @RequestParam(value = "sigma_rows_count", defaultValue = "10") String sigmaRowsCount,
                              @RequestParam(value = "sigma_rod_number", defaultValue = "1") String sigmaRodNumber,
                              Model model) {
        postProcessorProperties.setNxRowsCount(Integer.parseInt(nxRowsCount));
        double[][] matrixNx = postProcessorService.calculateNx(Integer.parseInt(nxRodNumber) - 1);

        postProcessorProperties.setUxRowsCount(Integer.parseInt(uxRowsCount));
        double[][] matrixUx = postProcessorService.calculateUx();

        postProcessorProperties.setSigmaRowsCount(Integer.parseInt(sigmaRowsCount));
        double[][] sigma = postProcessorService.calculateSigma(Integer.parseInt(sigmaRodNumber) - 1);

        model.addAttribute("matrixNx", matrixNx);
        model.addAttribute("matrixUx", matrixUx);
        model.addAttribute("sigma", sigma);

        return "postprocessor_page";
    }
}
