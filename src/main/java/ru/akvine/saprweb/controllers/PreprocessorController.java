package ru.akvine.saprweb.controllers;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.akvine.saprweb.domain.entities.Assembly;
import ru.akvine.saprweb.domain.entities.Rod;
import ru.akvine.saprweb.domain.models.AssembliesProperties;
import ru.akvine.saprweb.domain.models.PreProcessorProperties;
import ru.akvine.saprweb.domain.models.ProcessorProperties;
import ru.akvine.saprweb.domain.models.RodsProperties;
import ru.akvine.saprweb.services.AssemblyService;
import ru.akvine.saprweb.services.NumberCorrectorService;
import ru.akvine.saprweb.services.RodService;
import ru.akvine.saprweb.services.ValidatorService;


@Controller
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PreprocessorController {
    RodService rodService;

    AssemblyService assemblyService;

    NumberCorrectorService numberCorrectorService;

    ValidatorService<Rod> validatorService;

    RodsProperties rodsProperties;

    PreProcessorProperties preProcessorProperties;

    AssembliesProperties assembliesProperties;

    ProcessorProperties processorProperties;

    @Autowired
    public void setProcessorProperties(ProcessorProperties processorProperties) {
        this.processorProperties = processorProperties;
    }

    @Autowired
    public void setValidatorService(ValidatorService<Rod> validatorService) {
        this.validatorService = validatorService;
    }

    @Autowired
    public void setNumberCorrectorService(NumberCorrectorService numberCorrectorService) {
        this.numberCorrectorService = numberCorrectorService;
    }

    @Autowired
    public void setPreProcessorProperties(PreProcessorProperties preProcessorProperties) {
        this.preProcessorProperties = preProcessorProperties;
    }

    @Autowired
    public void setAssembliesProperties(AssembliesProperties assembliesProperties) {
        this.assembliesProperties = assembliesProperties;
    }

    @Autowired
    public void setRodsProperties(RodsProperties rodsProperties) {
        this.rodsProperties = rodsProperties;
    }

    @Autowired
    public void setAssemblyService(AssemblyService assemblyService) {
        this.assemblyService = assemblyService;
    }

    @Autowired
    public void setRodService(RodService rodService) {
        this.rodService = rodService;
    }

    @GetMapping("/preprocessor")
    public String getInputValuesPage(Model model) {
        model.addAttribute("rod", new Rod());
        model.addAttribute("assembly", new Assembly());
        model.addAttribute("rods", rodService.findAll());
        model.addAttribute("assemblies", assemblyService.findAll());
        model.addAttribute("left_column", preProcessorProperties.isLeftColumn());
        model.addAttribute("right_column", preProcessorProperties.isRightColumn());

        return "preprocessor_page";
    }

    @GetMapping("/schedule")
    public String getSchedulePage() {
        return "schedule";
    }

    @PostMapping("/new-rod")
    public String addNewRod(@ModelAttribute(value = "rod") Rod rod) {
        boolean isValidRod = validatorService.validateForm(rod);

        if (isValidRod) {
            rodService.save(rod);
        }

        return "redirect:/preprocessor";
    }

    @PostMapping("/new-assembly")
    public String addNewAssembly(@ModelAttribute(value = "assembly") Assembly assembly) {

        assemblyService.save(assembly);

        return "redirect:/preprocessor";
    }

    @PostMapping("/set-options")
    public String setOptions(@RequestParam(name = "left_column", required = false) boolean leftColumn,
                             @RequestParam(name = "right_column", required = false) boolean rightColumn,
                             @RequestParam(name = "rounding", required = false, defaultValue = "3") int rounding,
                             @RequestParam(name = "validate_data", required = false) boolean validateData,
                             @RequestParam(name = "length_coef", required = false, defaultValue = "Standard") String lengthCoef,
                             @RequestParam(name = "elasticity_module_coef", required = false, defaultValue = "M") String elasticityModuleCoef,
                             @RequestParam(name = "tension_coef", required = false, defaultValue = "М") String tensionCoef,
                             @RequestParam(name = "load_coef", required = false, defaultValue = "Standard") String rodLoadCoef,
                             @RequestParam(name = "sectional_coef", required = false, defaultValue = "Standard") String sectionalCoef,
                             @RequestParam(name = "load_coef_assembly", required = false, defaultValue = "Standard") String assemblyLoadCoef) {
        preProcessorProperties.setLeftColumn(leftColumn);
        preProcessorProperties.setRightColumn(rightColumn);
        processorProperties.setRounding(rounding);

        rodsProperties.setLengthCoef(lengthCoef);
        rodsProperties.setElasticityModuleCoef(elasticityModuleCoef);
        rodsProperties.setTenstionCoef(tensionCoef);
        rodsProperties.setLoadCoef(rodLoadCoef);
        rodsProperties.setSectionalCoef(sectionalCoef);

        assembliesProperties.setLoadCoeff(assemblyLoadCoef);

        return "redirect:/preprocessor";
    }

    @RequestMapping(value = "/delete-rod/{id}", method = RequestMethod.GET)
    public String deleteRd(@PathVariable("id") Long id) {

        rodService.delete(id);

        numberCorrectorService.correctRodsNumbers();

        return "redirect:/preprocessor";
    }

    @RequestMapping(value = "/delete-assembly/{id}", method = RequestMethod.GET)
    public String deleteAssembly(@PathVariable("id") Long id) {
        assemblyService.delete(id);

        numberCorrectorService.correctAssembliesNumbers();

        return "redirect:/preprocessor";
    }

    @GetMapping("edit-rod/{id}")
    public String updateRod(@PathVariable("id") Long id, Model model) {
        Rod rodToUpdate = rodService.findById(id).get();

        model.addAttribute("rod", rodToUpdate);

        return "edit_rod";
    }

    @GetMapping("edit-assembly/{id}")
    public String updateAssembly(@PathVariable("id") Long id, Model model) {
        Assembly assemblyToUpdate = assemblyService.findById(id).get();

        model.addAttribute("assembly", assemblyToUpdate);

        return "edit_assembly";
    }

    @PostMapping("/update-rod")
    public String updateRod(@ModelAttribute("rod") Rod rod) {
        rodService.update(rod.getId(), rod);

        numberCorrectorService.correctRodsNumbers();

        return "redirect:/preprocessor";
    }

    @PostMapping("/update-assembly")
    public String updateAssembly(@ModelAttribute("assembly") Assembly assembly) {
        assemblyService.update(assembly.getId(), assembly);

        numberCorrectorService.correctAssembliesNumbers();

        return "redirect:preprocessor";
    }


}
