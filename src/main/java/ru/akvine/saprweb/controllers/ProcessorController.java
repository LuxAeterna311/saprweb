package ru.akvine.saprweb.controllers;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.akvine.saprweb.domain.models.ProcessorProperties;
import ru.akvine.saprweb.services.ProcessorService;
import ru.akvine.saprweb.utils.MathUtil;

@Controller
@RequestMapping("/processor")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProcessorController {
    ProcessorService processorService;

    ProcessorProperties processorProperties;

    @Autowired
    public void setProcessorService(ProcessorService processorService) {
        this.processorService = processorService;
    }

    @Autowired
    public void setRodsProperties(ProcessorProperties processorProperties) {
        this.processorProperties = processorProperties;
    }

    @GetMapping
    public String getProcessorPage(Model model) {
        double[][] matrixA = processorService.calculateMatrixA();
        double[] vectorB = processorService.calculateVectorB();
        double[] delta = processorService.calculateDelta(MathUtil.cloneMatrix(matrixA), MathUtil.cloneVector(vectorB));
        double[][] Nx = processorService.calculateNx(MathUtil.cloneMatrix(matrixA), MathUtil.cloneVector(vectorB));
        double[][] U = processorService.calculateU(MathUtil.cloneMatrix(matrixA), MathUtil.cloneVector(vectorB));

        model.addAttribute("matrixA", MathUtil.roundMatrixValues(matrixA, processorProperties.getRounding()));
        model.addAttribute("vectorB", MathUtil.roundVectorValues(vectorB, processorProperties.getRounding()));
        model.addAttribute("delta", MathUtil.roundVectorValues(delta, processorProperties.getRounding()));
        model.addAttribute("Nx", MathUtil.roundMatrixValues(Nx, processorProperties.getRounding()));
        model.addAttribute("U", MathUtil.roundMatrixValues(U, processorProperties.getRounding()));

        return "processor_page";
    }
}
