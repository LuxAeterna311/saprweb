package ru.akvine.saprweb.domain.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "rods")
@NoArgsConstructor
public class Rod implements Serializable {

    public Rod(double length, double elasticityModule, double tension, double load, double sectional) {
        this.length = length;
        this.elasticityModule = elasticityModule;
        this.tension = tension;
        this.load = load;
        this.sectional = sectional;
    }

    public Rod(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    /**
     * Номер стержня.
     */
    @NotNull
    @Size(min = 0)
    long number;

    /**
     * Длина стержня.
     */
    @NotNull
    double length;

    /**
     * Модуль упругости.
     */
    @Positive
    double elasticityModule;

    /**
     * Дополнительное напряжение.
     */
    @Positive
    double tension;

    /**
     * Нагрузка.
     */
    double load;

    /**
     * Площадь поперечного сечения.
     */
    @Positive
    double sectional;
}
