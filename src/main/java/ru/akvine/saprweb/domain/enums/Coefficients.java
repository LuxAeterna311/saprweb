package ru.akvine.saprweb.domain.enums;

public enum Coefficients {
    T(12),
    G(9),
    M(6),
    K(3),
    Standard(0),
    d(-1),
    sm(-2),
    ml(-3),
    mk(-6),
    n(-9),
    p(-12);

    private int coefficient;

     Coefficients(int coefficient) {
        this.coefficient = coefficient;
    }

    public int getCoefficient() {
        return coefficient;
    }
}
