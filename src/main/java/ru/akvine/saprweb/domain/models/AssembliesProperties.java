package ru.akvine.saprweb.domain.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;

@Component
@Getter
@Setter
public class AssembliesProperties implements Serializable {
    String loadCoeff;

    @PostConstruct
    public void init() {
        this.loadCoeff = "Standard";
    }
}
