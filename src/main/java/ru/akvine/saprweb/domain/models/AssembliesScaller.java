package ru.akvine.saprweb.domain.models;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.akvine.saprweb.domain.entities.Assembly;
import ru.akvine.saprweb.domain.enums.Coefficients;
import ru.akvine.saprweb.domain.models.interfaces.Scaller;
import ru.akvine.saprweb.services.AssemblyService;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AssembliesScaller implements Scaller {
    AssemblyService assemblyService;

    List<Assembly> scalledAssemblies;

    AssembliesProperties assembliesProperties;

    @Autowired
    public void setAssemblyService(AssemblyService assemblyService) {
        this.assemblyService = assemblyService;
    }

    @Autowired
    public void setAssembliesProperties(AssembliesProperties assembliesProperties) {
        this.assembliesProperties = assembliesProperties;
    }

    @Autowired
    public List<Assembly> getScalledAssemblies() {
        return scalledAssemblies;
    }

    @Override
    public List<Assembly> scale() {
        boolean needsScalling = checkNeedsScalling();

        if (needsScalling) {
            List<Assembly> assemblies = assemblyService.findAll();
            int fieldsCount = assembliesProperties.getClass().getDeclaredFields().length;

            double[][] extractedMatrix = new double[assemblies.size()][fieldsCount];

            try {
                extractedMatrix = extract(assemblies.toArray());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            try {
                for (int i = 0; i < fieldsCount; ++i) {
                    for (int j = 0; j < extractedMatrix.length; ++j) {
                        extractedMatrix[j][i] = extractedMatrix[j][i] * Math.pow(10, getCoefficient(i));
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            scalledAssemblies = packExtractedValues(extractedMatrix);

            return scalledAssemblies;
        } else {
            scalledAssemblies = assemblyService.findAll();

            return scalledAssemblies;
        }
    }

    @Override
    public int getCoefficient(int column) throws IllegalAccessException {
        Field[] propertiesFields = assembliesProperties.getClass().getDeclaredFields();

        propertiesFields[column].setAccessible(true);
        String fieldValue = (String) propertiesFields[column].get(assembliesProperties);

        return Coefficients.valueOf(fieldValue).getCoefficient();
    }

    @Override
    public List<Assembly> packExtractedValues(double[][] extractedValues) {
        List<Assembly> assemblies = new ArrayList<>();

        try {
            for (int i = 0; i < extractedValues.length; ++i) {
                Assembly assembly = new Assembly();

                for (int j = 0; j < extractedValues[i].length; ++j) {
                    Field[] fields = assembly.getClass().getDeclaredFields();
                    fields[j + 2].setAccessible(true);
                    fields[j + 2].setDouble(assembly, extractedValues[i][j]);
                }

                assemblies.add(assembly);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return assemblies;
    }

    @Override
    public double[][] extract(Object[] assemblies) throws IllegalAccessException {
        int fieldsCount = assemblies[0].getClass().getDeclaredFields().length;
        double[][] extractedValues = new double[assemblies.length][fieldsCount - 2];

        for (int i = 0; i < assemblies.length; ++i) {
            Field[] objectFields = assemblies[i].getClass().getDeclaredFields();
            int k = 0;

            for (int j = 2; j < fieldsCount; ++j) {
                objectFields[j].setAccessible(true);
                extractedValues[i][k] = objectFields[j].getDouble(assemblies[i]);
                k++;
            }
        }

        return extractedValues;
    }

    @Override
    public boolean checkNeedsScalling() {
        Field[] propertiesFields = assembliesProperties.getClass().getDeclaredFields();

        for (int i = 0; i < propertiesFields.length; ++i) {
            propertiesFields[i].setAccessible(true);

            try {
                if (!propertiesFields[i].get(assembliesProperties).equals("Standard")) {
                    return true;
                }
            } catch (IllegalAccessException exception) {
                exception.printStackTrace();
            }
        }

        return false;
    }
}
