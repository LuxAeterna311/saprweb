package ru.akvine.saprweb.domain.models;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import ru.akvine.saprweb.domain.entities.Assembly;
import ru.akvine.saprweb.domain.entities.Rod;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Builder(toBuilder = true)
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class File implements Serializable {
    String name;

    List<Rod> rods;

    List<Assembly> assemblies;

    PreProcessorProperties preProcessorProperties;

    ProcessorProperties processorProperties;

    PostProcessorProperties postProcessorProperties;

    RodsProperties rodsProperties;

    AssembliesProperties assembliesProperties;

    LocalDate createdDate;
}
