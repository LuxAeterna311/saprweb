package ru.akvine.saprweb.domain.models;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.akvine.saprweb.domain.entities.Assembly;
import ru.akvine.saprweb.domain.entities.Rod;
import ru.akvine.saprweb.services.AssemblyService;
import ru.akvine.saprweb.services.RodService;

import java.time.LocalDate;
import java.util.List;

@Component
public class FileManager {
    RodService rodService;

    AssemblyService assemblyService;

    PreProcessorProperties preProcessorProperties;

    RodsProperties rodsProperties;

    AssembliesProperties assembliesProperties;

    ProcessorProperties processorProperties;

    PostProcessorProperties postProcessorProperties;

    @Autowired
    public void setProcessorProperties(ProcessorProperties processorProperties) {
        this.processorProperties = processorProperties;
    }

    @Autowired
    public void setRodsProperties(RodsProperties rodsProperties) {
        this.rodsProperties = rodsProperties;
    }

    @Autowired
    public void setAssembliesProperties(AssembliesProperties assembliesProperties) {
        this.assembliesProperties = assembliesProperties;
    }

    @Autowired
    public void setAssemblyService(AssemblyService assemblyService) {
        this.assemblyService = assemblyService;
    }

    @Autowired
    public void setRodService(RodService rodService) {
        this.rodService = rodService;
    }

    @Autowired
    public void setPreProcessorProperties(PreProcessorProperties preProcessorProperties) {
        this.preProcessorProperties = preProcessorProperties;
    }

    @Autowired
    public void setPostProcessorProperties(PostProcessorProperties postProcessorProperties) {
        this.postProcessorProperties = postProcessorProperties;
    }

    public File buildFile() {
        List<Rod> rods = rodService.findAll();
        List<Assembly> assemblies = assemblyService.findAll();

        return File.builder()
                .name("file")
                .rods(rods)
                .assemblies(assemblies)
                .createdDate(LocalDate.now())
                .assembliesProperties(assembliesProperties)
                .rodsProperties(rodsProperties)
                .preProcessorProperties(preProcessorProperties)
                .processorProperties(processorProperties)
                .postProcessorProperties(postProcessorProperties)
                .build();
    }

    public void writeData(File file) {
        List<Rod> rods = file.getRods();
        List<Assembly> assemblies = file.getAssemblies();

        rodService.saveAll(rods);
        assemblyService.saveAll(assemblies);

        preProcessorProperties.setLeftColumn(file.getPreProcessorProperties().isLeftColumn());
        preProcessorProperties.setRightColumn(file.getPreProcessorProperties().isRightColumn());

        processorProperties.setRounding(file.getProcessorProperties().getRounding());

        postProcessorProperties.setNxRowsCount(file.getPostProcessorProperties().getNxRowsCount());
        postProcessorProperties.setUxRowsCount(file.getPostProcessorProperties().getUxRowsCount());
        postProcessorProperties.setSigmaRowsCount(file.getPostProcessorProperties().getSigmaRowsCount());

        rodsProperties.setLengthCoef(file.getRodsProperties().getLengthCoef());
        rodsProperties.setElasticityModuleCoef(file.getRodsProperties().getElasticityModuleCoef());
        rodsProperties.setTenstionCoef(file.getRodsProperties().getTenstionCoef());
        rodsProperties.setLoadCoef(file.getRodsProperties().getLoadCoef());
        rodsProperties.setSectionalCoef(file.getRodsProperties().getSectionalCoef());

        assembliesProperties.setLoadCoeff(file.getAssembliesProperties().getLoadCoeff());
    }
}
