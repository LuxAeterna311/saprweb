package ru.akvine.saprweb.domain.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;

@Component
@Getter
@Setter
public class PostProcessorProperties implements Serializable {
    int NxRowsCount;
    int UxRowsCount;
    int sigmaRowsCount;

    int rounding;

    @PostConstruct
    public void init() {
        this.NxRowsCount = 10;
        this.UxRowsCount = 10;
        this.sigmaRowsCount = 10;
        this.rounding = 3;
    }
}
