package ru.akvine.saprweb.domain.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;

@Component
@Getter
@Setter
public class PreProcessorProperties implements Serializable {
    boolean leftColumn;

    boolean rightColumn;

    @PostConstruct
    public void init() {
        this.leftColumn = false;
        this.rightColumn = false;
    }
}
