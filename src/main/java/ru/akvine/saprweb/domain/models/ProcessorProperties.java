package ru.akvine.saprweb.domain.models;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;

@Component
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProcessorProperties implements Serializable {
    int rounding;

    @PostConstruct
    public void init() {
        this.rounding = 3;
    }
}
