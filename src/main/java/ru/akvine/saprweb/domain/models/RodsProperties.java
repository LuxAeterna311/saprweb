package ru.akvine.saprweb.domain.models;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;

@Component
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@NoArgsConstructor
public class RodsProperties implements Serializable {

    String lengthCoef;

    String elasticityModuleCoef;

    String tenstionCoef;

    String loadCoef;

    String sectionalCoef;


    @PostConstruct
    public void init() {
        this.lengthCoef = "Standard";
        this.elasticityModuleCoef = "M";
        this.tenstionCoef = "M";
        this.loadCoef = "Standard";
        this.sectionalCoef = "Standard";
    }

    public RodsProperties(String lengthCoef, String elasticityModuleCoef, String tenstionCoef, String loadCoef, String sectionalCoef) {
        this.lengthCoef = lengthCoef;
        this.elasticityModuleCoef = elasticityModuleCoef;
        this.tenstionCoef = tenstionCoef;
        this.loadCoef = loadCoef;
        this.sectionalCoef = sectionalCoef;
    }
}
