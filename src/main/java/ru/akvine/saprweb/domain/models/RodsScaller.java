package ru.akvine.saprweb.domain.models;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.akvine.saprweb.domain.entities.Rod;
import ru.akvine.saprweb.domain.enums.Coefficients;
import ru.akvine.saprweb.domain.models.interfaces.Scaller;
import ru.akvine.saprweb.services.RodService;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Component
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
public class RodsScaller implements Scaller {
    RodService rodService;

    RodsProperties rodsProperties;

    List<Rod> scalledRods;

    @Autowired
    public void setRodService(RodService rodService) {
        this.rodService = rodService;
    }

    @Autowired
    public void setRodsProperties(RodsProperties rodsProperties) {
        this.rodsProperties = rodsProperties;
    }

    public List<Rod> scale() {
        boolean needsScalling = checkNeedsScalling();

        if (needsScalling) {
            List<Rod> rods = rodService.findAll();
            int fieldsCount = rodsProperties.getClass().getDeclaredFields().length;

            double[][] extractedMatrix = new double[rods.size()][fieldsCount];

            try {
                extractedMatrix = extract(rods.toArray());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            try {
                for (int i = 0; i < fieldsCount; ++i) {
                    for (int j = 0; j < extractedMatrix.length; ++j) {
                        extractedMatrix[j][i] = extractedMatrix[j][i] * Math.pow(10, getCoefficient(i));
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            scalledRods = packExtractedValues(extractedMatrix);

            return scalledRods;
        } else {
            scalledRods = rodService.findAll();

            return scalledRods;
        }
    }

    public int getCoefficient(int column) throws IllegalAccessException {
        Field[] propertiesFields = rodsProperties.getClass().getDeclaredFields();

        propertiesFields[column].setAccessible(true);
        String fieldValue = (String) propertiesFields[column].get(rodsProperties);

        return Coefficients.valueOf(fieldValue).getCoefficient();
    }

    public List<Rod> packExtractedValues(double[][] extractedValues) {
        List<Rod> rods = new ArrayList<>();

        try {
            for (int i = 0; i < extractedValues.length; ++i) {
                Rod rod = new Rod();

                for (int j = 0; j < extractedValues[i].length; ++j) {
                    Field[] fields = rod.getClass().getDeclaredFields();
                    fields[j + 2].setAccessible(true);
                    fields[j + 2].setDouble(rod, extractedValues[i][j]);
                }

                rods.add(rod);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return rods;
    }

    public double[][] extract(Object[] rods) throws IllegalAccessException {
        int fieldsCount = rods[0].getClass().getDeclaredFields().length;
        double[][] extractedValues = new double[rods.length][fieldsCount - 2];

        for (int i = 0; i < rods.length; ++i) {
            Field[] objectFields = rods[i].getClass().getDeclaredFields();
            int k = 0;

            for (int j = 2; j < fieldsCount; ++j) {
                objectFields[j].setAccessible(true);
                extractedValues[i][k] = objectFields[j].getDouble(rods[i]);
                k++;
            }
        }

        return extractedValues;
    }

    public boolean checkNeedsScalling() {
        Field[] propertiesFields = rodsProperties.getClass().getDeclaredFields();

        for (int i = 4; i < propertiesFields.length; ++i) {
            propertiesFields[i].setAccessible(true);

            try {
                if (!propertiesFields[i].get(rodsProperties).equals("Standard")) {
                    return true;
                }
            } catch (IllegalAccessException exception) {
                exception.printStackTrace();
            }
        }

        return false;
    }
}
