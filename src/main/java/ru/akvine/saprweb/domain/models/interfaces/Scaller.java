package ru.akvine.saprweb.domain.models.interfaces;

import java.util.List;

public interface Scaller {
    List<?> scale();

    int getCoefficient(int column) throws IllegalAccessException;

    List<?> packExtractedValues(double[][] extractedValues);

    double[][] extract(Object[] objects) throws IllegalAccessException;

    boolean checkNeedsScalling();
}
