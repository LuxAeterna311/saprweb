package ru.akvine.saprweb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.akvine.saprweb.domain.entities.Assembly;

import java.util.List;

@Repository
public interface AssemblyRepository extends JpaRepository<Assembly, Long> {
    @Query(value = "SELECT number FROM Assembly ")
    List<Long> getAssembliesNumbers();

    @Query("UPDATE Assembly assembly " +
            "SET assembly.number = :number " +
            "WHERE assembly.id = :id")
    @Modifying
    void updateNumber(@Param("id") Long id, @Param("number") long number);
}
