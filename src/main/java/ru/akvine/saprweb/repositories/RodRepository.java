package ru.akvine.saprweb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.akvine.saprweb.domain.entities.Rod;

import java.util.List;

@Repository
public interface RodRepository extends JpaRepository<Rod, Long> {

    @Query(value = "SELECT number FROM Rod")
    List<Long> getAllNumbers();

    @Query("UPDATE Rod rod " +
            "SET rod.number = :number " +
            "WHERE rod.id = :id")
    @Modifying
    void updateNumber(@Param("id") Long id, @Param("number") long number);

    @Query("SELECT rod.length " +
            "FROM Rod as rod")
    double[] getLengths();

    @Query("SELECT rod.sectional " +
            "FROM Rod as rod")
    double[] getSectionals();

    @Query("SELECT rod.tension " +
            "FROM Rod as rod")
    double[] getTensions();
}
