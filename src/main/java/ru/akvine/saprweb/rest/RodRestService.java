package ru.akvine.saprweb.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.akvine.saprweb.domain.entities.Rod;
import ru.akvine.saprweb.services.RodService;

import java.util.List;

@RestController
public class RodRestService {

    RodService rodService;

    @Autowired
    public void setRodService(RodService rodService) {
        this.rodService = rodService;
    }

    @GetMapping("/rods")
    public List<Rod> getRods() {
        return rodService.findAll();
    }
}
