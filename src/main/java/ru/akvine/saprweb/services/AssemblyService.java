package ru.akvine.saprweb.services;

import ru.akvine.saprweb.domain.entities.Assembly;

import java.util.List;
import java.util.Optional;

public interface AssemblyService {
    Assembly save(Assembly assembly);

    Optional<Assembly> findById(Long id);

    List<Assembly> findAll();

    List<Long> getAssembliesNumbers();

    void delete(Long id);

    void updateCount();

    void updateNumber(Long id, long number);

    void saveAll(List<Assembly> assemblies);

    Assembly update(Long id, Assembly assembly);

    long count();
}
