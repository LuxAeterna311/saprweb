package ru.akvine.saprweb.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileService {
    void upload(MultipartFile resource) throws IOException, ClassNotFoundException;

    void download(String fileName) throws IOException;
}
