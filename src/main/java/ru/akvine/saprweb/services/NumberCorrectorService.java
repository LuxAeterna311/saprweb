package ru.akvine.saprweb.services;

import java.util.List;

public interface NumberCorrectorService {
   boolean checkNumbers(List<Long> numbers);

    void correctRodsNumbers();

    void correctAssembliesNumbers();
}
