package ru.akvine.saprweb.services;

public interface PostProcessorService {
    double[][] calculateNx(int rodNumber);

    double[][] calculateUx();

    double[][] calculateSigma(int rodNumber);
}
