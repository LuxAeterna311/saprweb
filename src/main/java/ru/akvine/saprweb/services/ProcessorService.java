package ru.akvine.saprweb.services;

public interface ProcessorService {
    double[][] calculateMatrixA();

    double[] calculateVectorB();

    double[] calculateDelta(double[][] matrixA, double[] vectorB);

    double[][] calculateNx(double[][] matrixA, double[] vectorB);

    double[][] calculateU(double[][] matrixA, double[] vectorB);

    double[][] getNx();

    double[][] getU();
}
