package ru.akvine.saprweb.services;

import ru.akvine.saprweb.domain.entities.Rod;

import java.util.List;
import java.util.Optional;

public interface RodService {
    Rod save(Rod rod);

    List<Rod> findAll();

    void delete(Long id);

    List<Long> getRodsNumbers();

    long count();

    void updateCount();

    void updateNumber(Long id, long number);

    void saveAll(List<Rod> rods);

    Rod update(Long id, Rod rod);

    Optional<Rod> findById(Long id);

    double[] getLengths();

    double[] getSectionals();

    double[] getTensions();
}
