package ru.akvine.saprweb.services;

public interface ValidatorService<T> {
    boolean validateForm(T object);
}
