package ru.akvine.saprweb.services.impl;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.akvine.saprweb.domain.entities.Assembly;
import ru.akvine.saprweb.repositories.AssemblyRepository;
import ru.akvine.saprweb.services.AssemblyService;

import java.util.List;
import java.util.Optional;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AssemblyServiceImpl implements AssemblyService {

    static long count = 0;

    private static final Logger LOGGER = LoggerFactory.getLogger(AssemblyServiceImpl.class);

    @Autowired
    AssemblyRepository assemblyRepository;

    @Override
    public Assembly save(Assembly assembly) {
        LOGGER.info("save {}", assembly);

        count++;
        assembly.setNumber(count);

        return assemblyRepository.save(assembly);
    }

    @Override
    public Optional<Assembly> findById(Long id) {
        return assemblyRepository.findById(id);
    }

    @Override
    public List<Assembly> findAll() {
        LOGGER.info("get all assemblies from DB");

        return assemblyRepository.findAll();
    }

    @Override
    public List<Long> getAssembliesNumbers() {
        return assemblyRepository.getAssembliesNumbers();
    }

    @Override
    public void delete(Long id) {
        assemblyRepository.deleteById(id);
    }

    @Override
    public void updateCount() {
        count = assemblyRepository.count();
    }

    @Override
    public void updateNumber(Long id, long number) {
        assemblyRepository.updateNumber(id, number);
    }

    @Override
    public void saveAll(List<Assembly> assemblies) {
        LOGGER.info("save all assemblies");

        assemblyRepository.saveAll(assemblies);
    }

    @Transactional
    @Override
    public Assembly update(Long id, Assembly assembly) {
        Assembly assemblyToUpdate = findById(id).get();

        assemblyToUpdate.setId(id);
        assemblyToUpdate.setNumber(assembly.getNumber());
        assemblyToUpdate.setLoad(assembly.getLoad());

        assemblyRepository.save(assemblyToUpdate);

        return assemblyToUpdate;
    }

    @Override
    public long count() {
        return assemblyRepository.count();
    }
}
