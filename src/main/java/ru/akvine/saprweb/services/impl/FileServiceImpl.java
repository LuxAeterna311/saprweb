package ru.akvine.saprweb.services.impl;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.akvine.saprweb.domain.models.File;
import ru.akvine.saprweb.domain.models.FileManager;
import ru.akvine.saprweb.services.FileService;

import java.io.*;
import java.time.LocalDate;
import java.util.Objects;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FileServiceImpl implements FileService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceImpl.class);

    FileManager fileManager;

    @Autowired
    public void setFileManager(FileManager fileManager) {
        this.fileManager = fileManager;
    }


    @Override
    public void upload(MultipartFile resource) throws IOException, ClassNotFoundException {
        LOGGER.info("upload file");

        FileInputStream fileInputStream = new FileInputStream(Objects.requireNonNull(resource.getOriginalFilename()));
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        File file = (File) objectInputStream.readObject();

        fileManager.writeData(file);
    }

    @Override
    public void download(String fileName) throws IOException {
        LOGGER.info("download file");

        File file = fileManager.buildFile();
        StringBuilder sb = new StringBuilder();
        String fileExtension = ".bin";

        if (fileName.equals("")) {
            fileName = sb.append("preprocessor_file").append(LocalDate.now()).toString();
            sb.setLength(0);
        }



        ObjectOutputStream objectOutputStream = null;

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new StringBuilder().append(fileName).append(fileExtension).toString());
            objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(file);

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            objectOutputStream.close();
        }
    }
}
