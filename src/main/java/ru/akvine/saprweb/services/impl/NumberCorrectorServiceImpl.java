package ru.akvine.saprweb.services.impl;


import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.akvine.saprweb.domain.entities.Assembly;
import ru.akvine.saprweb.domain.entities.Rod;
import ru.akvine.saprweb.services.AssemblyService;
import ru.akvine.saprweb.services.NumberCorrectorService;
import ru.akvine.saprweb.services.RodService;

import javax.transaction.Transactional;
import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class NumberCorrectorServiceImpl implements NumberCorrectorService {

    RodService rodService;

    AssemblyService assemblyService;

    private static final Logger LOGGER = LoggerFactory.getLogger(NumberCorrectorServiceImpl.class);

    @Autowired
    public void setRodService(RodService rodService) {
        this.rodService = rodService;
    }

    @Autowired
    public void setAssemblyService(AssemblyService assemblyService) {
        this.assemblyService = assemblyService;
    }

    @Override
    @Transactional
    public void correctRodsNumbers() {
        LOGGER.info("correct rods numbers");

        boolean isCorrectRodsNumbers = checkNumbers(rodService.getRodsNumbers());

        if (!isCorrectRodsNumbers) {
            List<Rod> rods = rodService.findAll();
            int i = 1;

            for (Rod rod : rods) {
                rodService.updateNumber(rod.getId(), i);
                i++;
            }

            rodService.updateCount();
        }
    }

    @Transactional
    @Override
    public void correctAssembliesNumbers() {
        LOGGER.info("correct assemblies numbers");

        boolean isCorrectAssembliesNumbers = checkNumbers(assemblyService.getAssembliesNumbers());

        if (!isCorrectAssembliesNumbers) {
            List<Assembly> assemblies = assemblyService.findAll();
            int i = 1;

            for (Assembly assembly : assemblies) {
                assemblyService.updateNumber(assembly.getId(), i);
                i++;
            }

            assemblyService.updateCount();
        }
    }

    @Override
    public boolean checkNumbers(List<Long> numbers) {
        LOGGER.info("check numbers");

        long difference = 1;

        for (int i = 0; i < numbers.size() - 1; ++i) {
            difference = numbers.get(i + 1) - numbers.get(i);

            if (difference != 1) {
                return false;
            }
        }

        return true;
    }
}
