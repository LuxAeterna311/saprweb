package ru.akvine.saprweb.services.impl;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.akvine.saprweb.domain.models.PostProcessorProperties;
import ru.akvine.saprweb.services.PostProcessorService;
import ru.akvine.saprweb.services.ProcessorService;
import ru.akvine.saprweb.services.RodService;
import ru.akvine.saprweb.utils.MathUtil;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PostPorcessorServiceImpl implements PostProcessorService {

    ProcessorService processorService;

    PostProcessorProperties postProcessorProperties;

    RodService rodService;

    @Autowired
    public void setRodService(RodService rodService) {
        this.rodService = rodService;
    }

    @Autowired
    public void setAppProperties(PostProcessorProperties postProcessorProperties) {
        this.postProcessorProperties = postProcessorProperties;
    }

    @Autowired
    public void setProcessorService(ProcessorService processorService) {
        this.processorService = processorService;
    }

    @Override
    public double[][] calculateNx(int rodNumber) {
        double[][] matrixNx = processorService.getNx();
        double[] rodsLength = rodService.getLengths();
        int rows = postProcessorProperties.getNxRowsCount();

        double[][] resultedMatrix = new double[rows][2];

        double split = rodsLength[rodNumber] / rows;

        double value;
        int j = 0;

        for (double x = 0; x < (rodsLength[rodNumber] - split); x += split) {
            value = matrixNx[rodNumber][0] + matrixNx[rodNumber][1] * x;

            resultedMatrix[j][0] = x;
            resultedMatrix[j][1] = value;

            j++;
        }

        return MathUtil.roundMatrixValues(resultedMatrix, postProcessorProperties.getRounding());
    }

    @Override
    public double[][] calculateUx() {
        double[] rodsLength = rodService.getLengths();
        double[][] u = processorService.getU();
        int rows = postProcessorProperties.getUxRowsCount();

        double len = 0;

        double[][] resultedMatrix = new double[rows][2];

        for (double length : rodsLength) {
            len += length;
        }

        double split = len / rows;

        int j = 0;
        double x = 0;
        double lastX = 0;

        double value;

        for (int i = 0; i < rows; ++i) {
            try {
                for (; x < rodsLength[i] + lastX; x += split) {
                    value = u[i][0] + u[i][1] * (x - lastX) + u[i][2] * (x - lastX) * (x - lastX);

                    resultedMatrix[j][0] = x;
                    resultedMatrix[j][1] = value;

                    j++;
                }
                lastX += rodsLength[i];
            } catch (IndexOutOfBoundsException exception) {
                System.err.println("Выход за пределы массива!");
            }
        }


        return MathUtil.roundMatrixValues(resultedMatrix, postProcessorProperties.getRounding());
    }

    @Override
    public double[][] calculateSigma(int rodNumber) {
        double[][] matrixNx = processorService.getNx();
        double[] rodsLength = rodService.getLengths();
        double[] rodsSectionals = rodService.getSectionals();
        double[] rodsTensions = rodService.getTensions();

        int rows = postProcessorProperties.getSigmaRowsCount();

        double split = rodsLength[rodNumber] / rows;

        double[][] resultedMatrix = new double[rows][3];

        int j = 0;

        double value;

        for (double x = 0; x < rodsLength[rodNumber] - split; x += split) {
            value = (matrixNx[rodNumber][0] + matrixNx[rodNumber][1] * x) / rodsSectionals[rodNumber];

            if (Math.abs(value) > rodsTensions[rodNumber]) {
                  resultedMatrix[j][2] = 1;
            }

            resultedMatrix[j][0] = x;
            resultedMatrix[j][1] = value;

            j++;
        }

        return MathUtil.roundMatrixValues(resultedMatrix, postProcessorProperties.getRounding());
    }
}
