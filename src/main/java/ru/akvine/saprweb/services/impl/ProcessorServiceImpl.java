package ru.akvine.saprweb.services.impl;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.akvine.saprweb.domain.entities.Assembly;
import ru.akvine.saprweb.domain.entities.Rod;
import ru.akvine.saprweb.domain.models.AssembliesScaller;
import ru.akvine.saprweb.domain.models.PreProcessorProperties;
import ru.akvine.saprweb.domain.models.RodsScaller;
import ru.akvine.saprweb.services.AssemblyService;
import ru.akvine.saprweb.services.ProcessorService;
import ru.akvine.saprweb.services.RodService;
import ru.akvine.saprweb.utils.MathUtil;

import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class ProcessorServiceImpl implements ProcessorService {
    PreProcessorProperties preProcessorProperties;

    RodService rodService;

    AssemblyService assemblyService;

    RodsScaller rodsScaller;

    AssembliesScaller assembliesScaller;

    double[][] Nx;

    double[][] U;

    @Autowired
    public void setAssembliesScaller(AssembliesScaller assembliesScaller) {
        this.assembliesScaller = assembliesScaller;
    }

    @Autowired
    public void setPreProcessorProperties(PreProcessorProperties preProcessorProperties) {
        this.preProcessorProperties = preProcessorProperties;
    }

    @Autowired
    public void setRodService(RodService rodService) {
        this.rodService = rodService;
    }

    @Autowired
    public void setAssemblyService(AssemblyService assemblyService) {
        this.assemblyService = assemblyService;
    }

    @Autowired
    public void setRodsScaller(RodsScaller rodsScaller) {
        this.rodsScaller = rodsScaller;
    }

    @Override
    public double[][] calculateMatrixA() {
        List<Rod> rods = rodsScaller.scale();
        double[][] matrixA = MathUtil.fillZeros(rods.size() + 1);

        for (int i = 0; i < (int) rodService.count(); ++i) {
            matrixA[i][i] += (rods.get(i).getSectional() * rods.get(i).getElasticityModule()) / rods.get(i).getLength();
            matrixA[i][i + 1] -= (rods.get(i).getSectional() * rods.get(i).getElasticityModule()) / rods.get(i).getLength();
            matrixA[i + 1][i] -= (rods.get(i).getSectional() * rods.get(i).getElasticityModule()) / rods.get(i).getLength();
            matrixA[i + 1][i + 1] += (rods.get(i).getSectional() * rods.get(i).getElasticityModule()) / rods.get(i).getLength();
        }

        if (preProcessorProperties.isLeftColumn()) {
            matrixA[0][0] = 1;
            matrixA[1][0] = 0;
            matrixA[0][1] = 0;
        }
        if (preProcessorProperties.isRightColumn()) {
            int length = matrixA.length - 1;
            matrixA[length][length] = 1;
            matrixA[length - 1][length] = 0;
            matrixA[length][length - 1] = 0;
        }

        return matrixA;
    }

    @Override
    public double[] calculateVectorB() {
        List<Rod> rods = rodsScaller.getScalledRods();
        List<Assembly> assemblies = assembliesScaller.scale();

        int count = rods.size() + 1;
        double[] vectorB = new double[count];

        for (int i = 0; i <= count - 1; ++i) {
            vectorB[i] += assemblies.get(i).getLoad();

            if (i != 0) {
                vectorB[i] += rods.get(i - 1).getLoad() * rods.get(i - 1).getLength() / 2;
            }
            if (i != rods.size()) {
                vectorB[i] += rods.get(i).getLoad() * rods.get(i).getLength() / 2;
            }
        }

        if (preProcessorProperties.isLeftColumn()) {
            vectorB[0] = 0;
        }
        if (preProcessorProperties.isRightColumn()) {
            vectorB[rods.size()] = 0;
        }


        return vectorB;
    }

    @Override
    public double[] calculateDelta(double[][] matrixA, double[] vectorB) {
        double[][] clonedMatrix = MathUtil.cloneMatrix(matrixA);

        return MathUtil.multiplyMatrixAndVector(MathUtil.invertMatrix(clonedMatrix), vectorB);
    }

    @Override
    public double[][] calculateNx(double[][] matrixA, double[] vectorB) {
        int rodsCount = (int) rodService.count();
        List<Rod> rods = rodService.findAll();

        double[][] invertedMatrixA = MathUtil.invertMatrix(MathUtil.cloneMatrix(matrixA));
        double[] v6 = MathUtil.multiplyMatrixAndVector(invertedMatrixA, vectorB);
        double[][] Nx = new double[rodsCount][2];

        for (int i = 0; i < rodsCount; ++i) {
            Nx[i][0] = (rods.get(i).getSectional() * rods.get(i).getElasticityModule() / rods.get(i).getLength()) * (v6[i + 1] - v6[i]);

            if (rods.get(i).getLoad() != 0) {
                Nx[i][0] += (rods.get(i).getLoad() * rods.get(i).getLength() /2);
                Nx[i][1] -= rods.get(i).getLoad();
            }
        }

        this.Nx = Nx;

        return Nx;
    }

    @Override
    public double[][] calculateU(double[][] matrixA, double[] vectorB) {
        int rodsCount = (int) rodService.count();
        List<Rod> rods = rodService.findAll();

        double[][] invertedMatrixA = MathUtil.invertMatrix(MathUtil.cloneMatrix(matrixA));
        double[] v6 = MathUtil.multiplyMatrixAndVector(invertedMatrixA, vectorB);
        double[][] U = new double[rodsCount][3];

        for (int i = 0; i < rodsCount; ++i) {
            U[i][0] = v6[i];
            U[i][1] = (v6[i + 1] - v6[i]) / rods.get(i).getLength();
            U[i][1] += (rods.get(i).getLoad() * rods.get(i).getLength()) / (2 * rods.get(i).getElasticityModule() * rods.get(i).getSectional());
            U[i][2] = -(rods.get(i).getLoad() / (2 * rods.get(i).getElasticityModule() * rods.get(i).getSectional()));
        }

       this.U = U;

        return U;
    }

    @Override
    public double[][] getNx() {
        return Nx;
    }

    @Override
    public double[][] getU() {
        return U;
    }
}
