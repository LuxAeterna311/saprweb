package ru.akvine.saprweb.services.impl;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.akvine.saprweb.domain.entities.Rod;
import ru.akvine.saprweb.repositories.RodRepository;
import ru.akvine.saprweb.services.RodService;

import java.util.List;
import java.util.Optional;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RodServiceImpl implements RodService {
    @Autowired
    RodRepository rodRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(RodServiceImpl.class);

    static long count = 0;

    @Override
    public Rod save(Rod rod) {
        LOGGER.info("save {}", rod);

        ++count;
        rod.setNumber(count);


        return rodRepository.save(rod);
    }

    @Override
    public List<Rod> findAll() {
        LOGGER.info("get all rods from DB");

        return rodRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        LOGGER.info("delete rod by id");

        --count;

        rodRepository.deleteById(id);
    }

    @Override
    public List<Long> getRodsNumbers() {
        return rodRepository.getAllNumbers();
    }

    @Override
    public long count() {
        return rodRepository.count();
    }

    @Override
    public void updateCount() {
        count = rodRepository.count();
    }

    @Override
    public void updateNumber(Long id, long number) {
        rodRepository.updateNumber(id, number);
    }

    @Override
    public void saveAll(List<Rod> rods) {
        rodRepository.saveAll(rods);
    }

    @Transactional
    @Override
    public Rod update(Long id, Rod rod) {
         Rod rodToUpdate = findById(id).get();

         rodToUpdate.setNumber(rod.getNumber());
         rodToUpdate.setLength(rod.getLength());
         rodToUpdate.setElasticityModule(rod.getElasticityModule());
         rodToUpdate.setLoad(rod.getLoad());
         rodToUpdate.setSectional(rod.getSectional());
         rodToUpdate.setTension(rod.getTension());

         save(rodToUpdate);

        return rodToUpdate;
    }

    @Override
    public Optional<Rod> findById(Long id) {
        return rodRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public double[] getLengths() {
        return rodRepository.getLengths();
    }

    @Override
    @Transactional(readOnly = true)
    public double[] getSectionals() {
        return rodRepository.getSectionals();
    }

    @Transactional(readOnly = true)
    @Override
    public double[] getTensions() {
        return rodRepository.getTensions();
    }
}
