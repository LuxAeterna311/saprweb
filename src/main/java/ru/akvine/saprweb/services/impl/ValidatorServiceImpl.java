package ru.akvine.saprweb.services.impl;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.akvine.saprweb.domain.entities.Rod;
import ru.akvine.saprweb.services.AssemblyService;
import ru.akvine.saprweb.services.RodService;
import ru.akvine.saprweb.services.ValidatorService;

@Component
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ValidatorServiceImpl implements ValidatorService<Rod> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatorServiceImpl.class);

    RodService rodService;

    AssemblyService assemblyService;

    @Autowired
    public void setRodService(RodService rodService) {
        this.rodService = rodService;
    }

    @Autowired
    public void setAssemblyService(AssemblyService assemblyService) {
        this.assemblyService = assemblyService;
    }

    @Override
    public boolean validateForm(Rod rod) {
        LOGGER.info("validate rod form");

        if (rod.getLength() <= 0) {
            return false;
        } else if (rod.getElasticityModule() <= 0) {
            return false;
        } else if (rod.getTension() <= 0) {
            return false;
        } else if (rod.getSectional() <= 0) {
            return false;
        } else {
            return true;
        }
    }
}
