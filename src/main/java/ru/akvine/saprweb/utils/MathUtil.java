package ru.akvine.saprweb.utils;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class MathUtil {

    public static double[][] fillZeros(int count) {
        double[][] matrixA = new double[count][count];

        for (int i = 0; i < count; ++i) {
            for (int j = 0; j < count; ++j) {
                matrixA[i][j] = 0;
            }
        }

        return matrixA;
    }

    public static double calculateDet(double[][] matrix) {
        double[][] temporary;
        double result = 0;

        if (matrix.length == 1) {
            result = matrix[0][0];
            return result;
        }

        for (int i = 0; i < matrix[0].length; i++) {
            temporary = new double[matrix.length - 1][matrix[0].length - 1];

            for (int j = 1; j < matrix.length; j++) {
                for (int k = 0; k < matrix[0].length; k++) {
                    if (k < i) {
                        temporary[j - 1][k] = matrix[j][k];
                    } else if (k > i) {
                        temporary[j - 1][k - 1] = matrix[j][k];
                    }
                }
            }

            result += matrix[0][i] * Math.pow (-1, (double) i) * calculateDet(temporary);
        }
        return result;
    }


    public static double[][] invertMatrix (double[][] matrix) {
        double[][] auxiliaryMatrix, invertedMatrix;
        int[] index;

        auxiliaryMatrix = new double[matrix.length][matrix.length];
        invertedMatrix = new double[matrix.length][matrix.length];
        index = new int[matrix.length];

        for (int i = 0; i < matrix.length; ++i) {
            auxiliaryMatrix[i][i] = 1;
        }

        transformToUpperTriangle (matrix, index);

        for (int i = 0; i < (matrix.length - 1); ++i) {
            for (int j = (i + 1); j < matrix.length; ++j) {
                for (int k = 0; k < matrix.length; ++k) {
                    auxiliaryMatrix[index[j]][k] -= matrix[index[j]][i] * auxiliaryMatrix[index[i]][k];
                }
            }
        }

        for (int i = 0; i < matrix.length; ++i) {
            invertedMatrix[matrix.length - 1][i] = (auxiliaryMatrix[index[matrix.length - 1]][i] / matrix[index[matrix.length - 1]][matrix.length - 1]);

            for (int j = (matrix.length - 2); j >= 0; --j) {
                invertedMatrix[j][i] = auxiliaryMatrix[index[j]][i];

                for (int k = (j + 1); k < matrix.length; ++k) {
                    invertedMatrix[j][i] -= (matrix[index[j]][k] * invertedMatrix[k][i]);
                }

                invertedMatrix[j][i] /= matrix[index[j]][j];
            }
        }

        return invertedMatrix;
    }

    public static void transformToUpperTriangle (double[][] matrix, int[] index) {
        double[] c;
        double c0, c1, pi0, pi1, pj;
        int itmp, k;

        c = new double[matrix.length];

        for (int i = 0; i < matrix.length; ++i) {
            index[i] = i;
        }

        for (int i = 0; i < matrix.length; ++i) {
            c1 = 0;

            for (int j = 0; j < matrix.length; ++j) {
                c0 = Math.abs (matrix[i][j]);

                if (c0 > c1) {
                    c1 = c0;
                }
            }

            c[i] = c1;
        }

        k = 0;

        for (int j = 0; j < (matrix.length - 1); ++j) {
            pi1 = 0;

            for (int i = j; i < matrix.length; ++i) {
                pi0 = Math.abs (matrix[index[i]][j]);
                pi0 /= c[index[i]];

                if (pi0 > pi1) {
                    pi1 = pi0;
                    k = i;
                }
            }

            itmp = index[j];
            index[j] = index[k];
            index[k] = itmp;

            for (int i = (j + 1); i < matrix.length; ++i) {
                pj = matrix[index[i]][j] / matrix[index[j]][j];
                matrix[index[i]][j] = pj;

                for (int l = (j + 1); l < matrix.length; ++l) {
                    matrix[index[i]][l] -= pj * matrix[index[j]][l];
                }
            }
        }
    }

    public static double[] multiplyMatrixAndVector(double[][] matrix, double[] vector) {
        double[] returnedVector = new double[matrix.length];

        if (matrix.length != vector.length) {
            return returnedVector;
        } else {
            for (int i = 0; i < matrix.length; ++i) {
                for (int j = 0; j < matrix.length; ++j) {
                    returnedVector[i] += matrix[i][j] * vector[j];
                }
            }
        }

        return returnedVector;
    }

    public static double[] roundVectorValues(double[] vector, int round) {
        double scale = Math.pow(10, round);

        for (int i = 0; i < vector.length; ++i) {
            vector[i] = Math.round(vector[i] * scale) / scale;
        }

        return vector;
    }

    public static double[][] roundMatrixValues(double[][] matrix, int round) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("%.");
        stringBuilder.append(round);
        stringBuilder.append("g%n");

        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[0].length; ++j) {
                matrix[i][j] = Double.parseDouble((String.format(stringBuilder.toString(), matrix[i][j])).replace(',', '.'));
            }
        }

        return matrix;
    }

    public static double[][] cloneMatrix(double[][] matrix) {
        double[][] clonedMatrix = new double[matrix.length][matrix.length];

        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix.length; ++j) {
                clonedMatrix[i][j] = matrix[i][j];
            }
        }

        return clonedMatrix;
    }

    public static double[] cloneVector(double[] vector) {
        double[] clonedVector = new double[vector.length];

        for (int i = 0; i < vector.length; ++i) {
            clonedVector[i] = vector[i];
        }

        return clonedVector;
    }

}
