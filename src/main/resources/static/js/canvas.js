async function fetchDataAsync(url) {
    const response = await fetch(url);
    let data = await response.json();

    let sectionals = [];


    for (let key in data) {
        let json = JSON.parse(JSON.stringify(data[key]));
        sectionals.push(json['sectional']);
    }

    draw_rods(sectionals);
}

function draw_rods(sectionals) {
    let canvas = document.getElementById('canvas');

    let context = canvas.getContext('2d');

    for (let i = 0; i < sectionals.length; ++i) {
        context.rect(100 * i, 100, 100, sectionals[i]);
        context.stroke();
    }
}

fetchDataAsync('http://localhost:8080/rods');
