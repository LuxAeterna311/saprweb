function validateForm() {
    let length = document.getElementById('length').value;
    let elasticityModule = document.getElementById('elasticityModule').value;
    let tension = document.getElementById('tension').value;
    let sectional = document.getElementById('sectional').value;

    if (length <= 0) {
        alert("Введите значение для длины больше 0");
        return false;
    }
    else if (elasticityModule <= 0) {
        alert("Введите значение для модуля упругости больше 0");
        return false;
    }
    else if (tension <= 0) {
        alert("Введите значение для напряжения больше 0");
        return false;
    }
    else if (sectional <= 0) {
        alert("Введите значение для площади поперечного сечения больше 0");
        return false;
    }
    else {
        return true;
    }

}
