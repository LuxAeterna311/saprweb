package ru.akvine.saprweb.domain.enums;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CoefficientsTest {

    @Test
    public void getCoefficientTest() {
        Assertions.assertEquals(12, Coefficients.valueOf("T").getCoefficient());
        Assertions.assertEquals(9, Coefficients.valueOf("G").getCoefficient());
        Assertions.assertEquals(6, Coefficients.valueOf("M").getCoefficient());
        Assertions.assertEquals(3, Coefficients.valueOf("K").getCoefficient());
        Assertions.assertEquals(0, Coefficients.valueOf("Standard").getCoefficient());
        Assertions.assertEquals(-1, Coefficients.valueOf("d").getCoefficient());
        Assertions.assertEquals(-2, Coefficients.valueOf("sm").getCoefficient());
        Assertions.assertEquals(-3, Coefficients.valueOf("ml").getCoefficient());
        Assertions.assertEquals(-6, Coefficients.valueOf("mk").getCoefficient());
        Assertions.assertEquals(-9, Coefficients.valueOf("n").getCoefficient());
        Assertions.assertEquals(-12, Coefficients.valueOf("p").getCoefficient());
    }
}
