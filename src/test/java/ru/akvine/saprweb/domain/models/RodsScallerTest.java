package ru.akvine.saprweb.domain.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.akvine.saprweb.domain.entities.Rod;

import java.util.Arrays;
import java.util.List;

public class RodsScallerTest {
    @Test
    public void extract() throws IllegalAccessException {
        List<Rod> rods1 = Arrays.asList(
                new Rod(1, 2, 1, 2, 3),
                new Rod(1, 3, 1, 2, 3),
                new Rod(1, 4, 1, 2, 3),
                new Rod(1, 5, 1, 2, 3)
        );
        List<Rod> rods2 = Arrays.asList(
                new Rod(1, 2, 3, 4, 5),
                new Rod(6, 7, 8, 9, 10),
                new Rod(11, 12, 13, 14, 15),
                new Rod(16, 17, 18, 19, 20)
        );

        RodsScaller rodsScaller = new RodsScaller();
        double[][] expectedValues1 = {{1, 2, 1, 2, 3}, {1, 3, 1, 2, 3}, {1, 4, 1, 2, 3}, {1, 5, 1, 2, 3}};
        double[][] expectedValues2 = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}};

        Assertions.assertArrayEquals(expectedValues1, rodsScaller.extract(rods1.toArray()));
        Assertions.assertArrayEquals(expectedValues2, rodsScaller.extract(rods2.toArray()));
    }
}
