package ru.akvine.saprweb.services.impl;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.akvine.saprweb.domain.entities.Rod;
import ru.akvine.saprweb.services.NumberCorrectorService;
import ru.akvine.saprweb.services.RodService;

import java.util.List;

import static org.mockito.Mockito.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@ExtendWith(SpringExtension.class)
@SpringBootTest
class NumberCorrectorServiceImplTest {

    @Autowired
    NumberCorrectorService numberCorrectorService;

    @Mock
    RodService rodService;

    @Test
    void correctRodsNumbersTrue() {
        when(rodService.getRodsNumbers()).thenReturn(List.of(2L, 1L, 3L));
        when(rodService.findAll()).thenReturn(List.of(new Rod(), new Rod()));

        numberCorrectorService.correctRodsNumbers();

        verify(rodService, times(1)).findAll();
        verify(rodService, times(1)).updateCount();
    }

    @Test
    void correctAssembliesNumbers() {
    }

    @Test
    void checkNumbers() {
    }
}