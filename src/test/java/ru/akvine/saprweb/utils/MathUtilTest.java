package ru.akvine.saprweb.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MathUtilTest {

    @Test
    public void calculateDet() {
        double[][] matrix1 = {{1, 2}, {3, 1}};
        double[][] matrix2 = {{9, 2, -12}, {4, 1, 1}, {9, 0, -23.1}};
        double[][] matrix3 = {{19, -6, 11, 9}, {-20, 10, 12, 1}, {0, 7, 2, 1}, {9, -3, -1, 0}};

        Assertions.assertEquals(-5, MathUtil.calculateDet(matrix1));
        Assertions.assertEquals(102.9, MathUtil.calculateDet(matrix2));
        Assertions.assertEquals(4548, MathUtil.calculateDet(matrix3));
    }

    @Test
    public void multiplyMatrixAndVector() {
        double[][] matrix1 = {{1, 2}, {3, 4}};
        double[] vector1 = {1, 2};
        double[] expectedVector1 = {5, 11};

        double[][] matrix2 = {{1, -5, 2}, {9, 0, 1}, {7, -2, 3}};
        double[] vector2 = {9, 1, 1};
        double[] expectedVector2 = {6, 82, 64};

        Assertions.assertArrayEquals(expectedVector1, MathUtil.multiplyMatrixAndVector(matrix1, vector1));
        Assertions.assertArrayEquals(expectedVector2, MathUtil.multiplyMatrixAndVector(matrix2, vector2));
    }

}
